from django import forms
from .models import SurveyFile, ImportConfig, ReferenceAlias
from asf.reference import Reference


class SurveyFileForm(forms.ModelForm):
    user = forms.HiddenInput()
    created_at = forms.HiddenInput()

    class Meta:
        model = SurveyFile
        fields = ["project", "file"]


class ReferenceAliasForm(forms.ModelForm):

    # def __init__(self, country):
    #     self.replace = forms.SelectMultiple(choices=Reference(country).get_names())
    # self.value = forms.TextInput(default=value)

    class Meta:
        model = ReferenceAlias
        fields = ["select", "replace"]


class ImportConfigForm(forms.ModelForm):
    # project = forms.ModelChoiceField(
    #     queryset=FormProject.objects.all(), widget=forms.HiddenInput()
    # )
    # survey_file = forms.ModelChoiceField(
    #     queryset=SurveyFile.objects.all(), widget=forms.HiddenInput()
    # )

    class Meta:
        model = ImportConfig
        fields = ["project", "survey_file"]
