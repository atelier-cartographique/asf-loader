import json

from django.http import HttpRequest, JsonResponse, HttpResponseBadRequest
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.decorators.cache import cache_page
from openpyxl import load_workbook

from asf.reference import Reference
from asf.models import (
    FIELD_TEXT,
    ImportConfig,
    ReferenceAlias,
    SurveyFile,
    FormProject,
    Row,
    Cell,
    FIELD_GEOMETRY,
    FIELD_NUMBER,
    FIELD_TYPE_CHOICES,
    GEOMETRY_CELL_NAME,
    CellGeom,
    CellNumber,
    CellText,
    CellError,
)
from asf.forms import ReferenceAliasForm


class Left:
    def __init__(self, note) -> None:
        self.note = note


class Right:
    def __init__(self, feature) -> None:
        self.feature = feature


def get_types():
    return [t[0] for t in FIELD_TYPE_CHOICES]


def from_python_type(row_t):
    t = str(row_t)
    if t == "<class 'str'>":
        return "text"
    elif t == "<class 'int'>":
        return "number"
    elif t == "<class 'boolean'>":
        return "boolean"
    elif t == "<class 'geometry'>":
        return "geom"
    elif t == "<class 'datetime.datetime'>":
        return "datetime"
    else:
        return t


def process_row(config, fields, ref, source_row, source_row_index, aliases):
    # print("Loading source row", source_row_index)

    cells = []
    try:
        row = Row.objects.create(config=config, index=source_row_index, status="draft")
        for name, ty, index in fields:
            value = source_row[index].value
            if ty == FIELD_GEOMETRY:
                try:

                    try:
                        alias = aliases.get(select=value)
                        geom = ref.lookup(alias.replace)
                        # print(f"REF-alias: {ref.lookup(alias.replace)}")
                    except ReferenceAlias.DoesNotExist:
                        geom = ref.lookup(value)
                        # print(f"REF; {ref.lookup(value)}")
                    ct = CellText(name, value)
                    ct.row = row
                    cells.append(ct)
                    cg = CellGeom(GEOMETRY_CELL_NAME, geom)
                    cg.row = row
                    cells.append(cg)
                except Reference.NotFound as nf:
                    # print(f"  <{name} - Reference not found", str(value)[:64])
                    c_name = CellError("geom", value)
                    c_name.row = row
                    cells.append(c_name)
                    c_geom = CellError(GEOMETRY_CELL_NAME, "Not found")
                    c_geom.row = row
                    cells.append(c_geom)

            elif ty == FIELD_TEXT:
                c = CellText(name, value)
                c.row = row
                cells.append(c)
            elif ty == FIELD_NUMBER:
                c = CellNumber(name, value)
                c.row = row
                cells.append(c)
            else:
                raise Exception(f"Failed on row {source_row_index}: {name} / {ty}")

        if len(fields) != len(cells) - 1:
            cols = "|".join([c.name for c in cells])
            print(
                "Incomplete row, skipping",
                source_row_index,
                cols,
            )
            return Left(f"Incomplete row {source_row_index}, {cols}")

        # print("<row", row.index)

        for cell in cells:
            try:
                # import pdb

                # pdb.set_trace()
                cell.save()
                # print(f"  <{cell.name}", str(cell.value)[:64])
            except Exception as ex:
                c = CellError(cell.name, cell.value)
                c.row = row
                c.save()
                # print(f"  <{cell.name} - {ex}", str(cell.value)[:64])

        return Right(row.get_feature())

        # print(f'Reference Not Found "{name}". Pick one?')
        # names = ref.get_names()
        # for i, candidate in enumerate(names):
        #     print(i, f": {candidate}")
        # index = int(input("? "))
        # aliases[name] = names[index]
        # return process_row(config, fields, ref, source_row, source_row_index, aliases)
    except Exception as ex:
        return Left((source_row_index, str(ex)))
        # print("Failed on row", source_row_index, str(ex))


def find_col_index(name, headers):
    for i, header in enumerate(headers):
        if name == header.strip():
            return i
    hns = [h.value for h in headers]
    raise Exception(f'Header Not Found: "{name}" in headers {hns}')


def get_header_names(headers):
    return [h.value for h in headers]


def get_fields(survey_file: SurveyFile):
    file = survey_file.file
    # print("Loading", file.name)
    wb = load_workbook(filename=file)
    if wb.active is None:
        raise Exception("No Active Sheet")

    # print("Loading rows")
    sheet = wb.active
    headers = list(sheet.iter_rows(max_row=1))[0]
    header_names = get_header_names(headers)
    row1 = list(sheet.iter_rows(max_row=2))[1]
    types = []
    for cel in row1:
        types.append(from_python_type(type(cel.value)))

    return (header_names, types)


def make_rows(project_id):
    aliases = ReferenceAlias.objects.all()
    config = ImportConfig.objects.get(project=project_id)
    config_file = SurveyFile.objects.filter(project=project_id).last()
    project = FormProject.objects.get(pk=project_id)
    if config_file is not None:
        wb = load_workbook(filename=config_file.file)
        sheet = wb.active
        if sheet is None:
            raise Exception("No Active Sheet")
        fields = [
            (name, ty, find_col_index(name, config.columns))
            for name, ty in config.get_fields()
        ]
        country = project.country
        ref = Reference(country)

        for source_row_index, source_row in enumerate(sheet.iter_rows(min_row=2)):
            process_row(config, fields, ref, source_row, source_row_index, aliases)


def get_preview_data(project_id):
    config = ImportConfig.objects.get(project=project_id)
    Row.objects.filter(config=config, status="draft").delete()
    make_rows(project_id)

    rows = Row.objects.filter(config=config, status="draft")
    data = []
    geom_error_names = set()
    other_errors = []
    for row in rows:

        cells = []
        for cell in row.get_cells():
            if cell.name != GEOMETRY_CELL_NAME:
                if cell.type == "error":
                    if cell.name == "geom":
                        geom_error_names.add(cell.value)
                        cells.append([(cell.value, "error-geom")])
                    else:
                        other_errors.append(cell.value)
                        cells.append([(cell.value, "error")])

                else:
                    cells.append([(cell.value, "validated")])
        data.append(cells)
    return (data, list(geom_error_names), other_errors)


def get_geojson_summary(
    project_id,
):
    # import pdb

    # pdb.set_trace()
    config = get_object_or_404(ImportConfig, project=project_id)
    choices = {}
    geom_field = config.get_geometry_field()

    for cell in Cell.objects.filter(
        ~Q(name=geom_field), type="text", row__config=config
    ):
        if cell.name not in choices:
            choices[cell.name] = set()
        choices[cell.name].add(cell.text)

    places = {}
    row_query = Row.objects.filter(config=config)
    for row in row_query:
        place = row.get_cell(geom_field).value
        if place not in places:
            places[place] = []
        places[place].append(row)

    features = []
    for index, (place, rows) in enumerate(places.items()):
        init = [["place", place], ["total", len(rows)]]
        for name, values in choices.items():
            for value in values:
                init.append([f"{name}/{value}", 0])
        props = dict(init)
        for name, ty in config.get_fields():
            if ty == "text":
                for row in rows:
                    cell = row.get_cell(name)
                    key = f"{name}/{cell.value}"
                    props[key] += 1
            if ty == "number":
                props[name] = 0
                for row in rows:
                    cell = row.get_cell(name)
                    props[name] += cell.value
        for key in props.keys():
            if props[key] == 0:
                props[key] == None
        features.append(
            dict(
                type="Feature",
                id=index,
                properties=props,
                geometry=json.loads(rows[0].get_geometry().geojson),
            )
        )

    return dict(type="FeatureCollection", features=features)


def get_geojson_topic(project_id, column, value):
    # import pdb

    # pdb.set_trace()
    config = get_object_or_404(ImportConfig, project=project_id)

    rows = Row.objects.filter(
        config=config,
        cells__type=FIELD_TEXT,
        cells__name=column,
        cells__text=value,
    )

    places = {}

    for row in rows:
        place = row.get_place()
        if place not in places:
            places[place] = []
        places[place].append(row)

    features = []

    for index, (place, rows) in enumerate(places.items()):
        init = [["place", place], ["total", len(rows)]]
        props = dict(init)
        for name, ty in config.get_fields():
            if name != column:
                props[name] = "; ".join(
                    list(set([str(row.get_cell(name).value) for row in rows]))
                )
        features.append(
            dict(
                type="Feature",
                id=index,
                properties=props,
                geometry=json.loads(rows[0].get_geometry().geojson),
            )
        )

    return dict(type="FeatureCollection", features=features)


@cache_page(60 * 60 * 24)
def get_geojson(request: HttpRequest, config_id, kind):
    # import pdb

    # pdb.set_trace()
    config = ImportConfig.objects.get(pk=config_id)
    project_id = config.project_id
    if kind == "summary":
        return JsonResponse(get_geojson_summary(project_id))
    elif kind == "topic":
        column = request.GET.get("column")
        value = request.GET.get("value")
        return JsonResponse(get_geojson_topic(project_id, column, value))

    return HttpResponseBadRequest(f'`kind` can be "summary" or "topic", not "{kind}"')
