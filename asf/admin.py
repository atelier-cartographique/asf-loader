from django.contrib import admin
from asf.models import *

admin.site.register(FormProject)
admin.site.register(ImportConfig)
admin.site.register(SurveyFile)
admin.site.register(Row)
admin.site.register(ReferenceAlias)
# admin.site.register(CellText)
# admin.site.register(CellBoolean)
# admin.site.register(CellNumber)
# admin.site.register(CellDateTime)
# admin.site.register(CellGeom)
