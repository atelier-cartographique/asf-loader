from django.shortcuts import render
from django.db.models import Q
from django.http import HttpResponseNotFound

from asf.views.data import (
    get_fields,
    get_preview_data,
    get_types,
)
from asf.forms import SurveyFileForm, ImportConfigForm
from asf.models import ImportConfig, FormProject, SurveyFile, ReferenceAlias, Row
from asf.reference import Reference

from api.models.metadata import MetaData, BoundingBox


# First upload step: upload file
def get_step_upload(request, error=""):
    survey_file_form = SurveyFileForm()
    return render(
        request,
        "asf/1-step-upload.html",
        {"survey_file_form": survey_file_form, "error": error},
    )


def post_upload(request):
    # make a SurveyFile from request
    # make a context out of file content + SurveyFile reference
    if request.method == "POST":
        file_form = SurveyFileForm(request.POST, request.FILES)

        if file_form.is_valid():
            file_post = file_form.save(commit=False)
            file_post.user = request.user
            file_post.save()

            try:
                ImportConfig.objects.get(
                    Q(project=file_post.project) | Q(survey_file=file_post)
                )
                return get_preview(request, file_post.project.id)

            except ImportConfig.DoesNotExist:
                return get_step_config(request, file_post)
        else:
            return get_step_upload(request, file_form.errors)
    return get_step_upload(request)


# Second upload step: choose columns and types for the config
def get_step_config(request, file):
    headers, types = get_fields(file)
    fields = zip(headers, types)
    form = ImportConfigForm(
        initial={
            "project": file.project,
            "survey_file": file,
        }
    )
    context = {
        "form": form,
        "types": get_types(),
        "fields": fields,
        "columns": headers,
        "project_name": file.project.name,
        "file_name": file.file.name,
    }

    return render(request, "asf/2-step-config.html", context)


# Third step: see preview (with possible errors) and add aliasses for location when needed
def post_config(request):
    # import pdb

    # pdb.set_trace()
    if request.method == "POST":

        fileId = request.POST["survey_file"]
        project_id = request.POST["project"]
        project = FormProject.objects.get(pk=project_id)
        file = SurveyFile.objects.get(pk=fileId)
        types = request.POST.getlist("types")
        columns = request.POST.getlist("columns")

        ImportConfig.objects.get_or_create(
            survey_file=file, types=types, columns=columns, project=project
        )
        return get_preview(request, project_id)
    return get_step_upload(request)


def get_preview(request, project_id):
    data, geom_errors, type_errors = get_preview_data(project_id)
    project = FormProject.objects.get(pk=project_id)
    references = Reference(project.country).get_names()
    config = ImportConfig.objects.get(project=project_id)
    # import pdb

    # pdb.set_trace()
    context = {
        "project_id": project_id,
        "columns": config.columns,
        "data": data,
        "geom_errors": geom_errors,
        "references": references,
        "type_errors": type_errors,
    }
    return render(request, "asf/3-preview.html", context)


def post_alias(request):
    if request.method == "POST":
        project_id = request.POST["project"]
        replaces = request.POST.getlist("replaces")
        selects = request.POST.getlist("selects")

        for i, r in enumerate(replaces):
            a = ReferenceAlias(replace=r, select=selects[i])
            a.save()
        # import pdb

        # pdb.set_trace()
        return get_preview(request, project_id)


def create_metadata_summary(project):
    title = {"fr": f"{project.name}/summary", "nl": f"{project.name}/summary"}
    abstract = {
        "fr": f"Synthèse pour le projet {project.name}",
        "en": f"Summary for {project.name} project",
    }

    resource_identifier = f"asf://summary/{project.config.id}"
    geometry_type = "MultiPolygon"

    metadata = MetaData.objects.create(
        title=title,
        abstract=abstract,
        resource_identifier=resource_identifier,
        geometry_type=geometry_type,
        bounding_box=BoundingBox.objects.create(),
    )
    return metadata


def create_metadata_topic(project: FormProject, column: str, value: str):
    config = ImportConfig.objects.get(project=project.id)
    title = {
        "fr": f"{project.name}:{column}/{value}",
        "nl": f"{project.name}:{column}/{value}",
    }
    abstract = {
        "fr": f"Synthèse pour le projet {project.name}",
        "en": f"Summary for {project.name} project",
    }

    resource_identifier = f"asf://topic/{config.id}?column={column}&value={value}"
    geometry_type = "MultiPolygon"

    metadata = MetaData.objects.create(
        title=title,
        abstract=abstract,
        resource_identifier=resource_identifier,
        geometry_type=geometry_type,
        bounding_box=BoundingBox.objects.create(),
    )
    return metadata


# def create_metadata(request):
#     user = request.user
#     project_id = request.POST["project"]
#     project = FormProject.objects.get(pk=project_id)
#     kind = request.POST["kind"]
#     if kind == "topic":
#         column = request.POST["column"]
#         value = request.POST["value"]
#         metadata = create_metadata_topic(project, user, column, value)
#     else:
#         metadata = create_metadata_summary(project, user)

#     context = {"metadataUrl": metadata.resource_identifier}
#     return render(request, "asf/confirmation.html", context)


def data_from_features(data):
    # import pdb

    # pdb.set_trace()
    features = data["features"]
    data = []
    for f in features:
        data.append(f["properties"].values())
    return data


def column_values(column: str, rows):
    [r.get_cell(column).value for r in rows]


def save_rows(request):
    project_id = request.POST["project"]
    project = FormProject.objects.get(pk=project_id)

    config = ImportConfig.objects.get(project=project_id)
    draft_rows = Row.objects.filter(config=config.id, status="draft")

    if len(draft_rows) > 0:
        Row.objects.filter(config=config, status="current").delete()
        for row in draft_rows:
            row.status = "current"
            row.save()

        # create metadata for summary
        # feature_collection_summary = get_geojson_summary(project_id)
        metadata_summary = create_metadata_summary(project)
        metadata_topic = []

        # create metadata for topics
        columns = request.POST.getlist("columns")
        for column in columns:
            values = list(set([r.get_cell(column).value for r in draft_rows]))
            # import pdb

            # pdb.set_trace()
            for value in values:
                # feature_collection_topic = get_geojson_topic(project_id, column, value)
                metadata = create_metadata_topic(project, column, value)
                metadata_props = {
                    "title": metadata.title["fr"],
                    "url": metadata.resource_identifier,
                }
                metadata_topic.append(metadata_props)

        # data = data_from_features(feature_collection_summary)
        # columns = feature_collection_summary["features"][0]["properties"]
        # context = {"data": data, "columns": columns, "project": project_id}
        context = {
            "metadata_summary": {
                "title": metadata_summary.title["fr"],
                "url": metadata_summary.resource_identifier,
            },
            "metadata_topic": metadata_topic,
        }
        return render(request, "asf/4-confirmation.html", context)
    return HttpResponseNotFound("No rows found")


# render(
#         request,
#         "asf/3-preview.html",
#         {project: project_id},
#     )
