from setuptools import setup, find_packages

version = "1.0.0"

name = "asf"
description = "load django app for ASF"
url = "https://atelier-cartographique/asf-loader.git"
author = "Atelier Cartographique"
author_email = "contact@atelier-cartographique.be"
license = "Affero GPL3"

classifiers = [
    "Development Status :: 1 - Alpha",
    "Topic :: Software Development :: Application",
    "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
    "Operating System :: POSIX",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3.8",
]

install_requires = ["django", "openpyxl"]

packages = find_packages()

setup(
    name=name,
    version=version,
    url=url,
    license=license,
    description=description,
    author=author,
    author_email=author_email,
    packages=packages,
    install_requires=install_requires,
    classifiers=classifiers,
    include_package_data=True,
)
