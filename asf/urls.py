from urllib.parse import urlencode, parse_qs
from django.urls import path, reverse
from asf.views.upload import (
    get_step_upload,
    post_upload,
    post_config,
    get_step_config,
    post_alias,
    save_rows,
    get_preview,
)
from asf.views.data import get_geojson


def prefix(s, sep="/"):
    return f"asf{sep}{s}"


def with_query(out, rid):
    if len(rid.query) > 0:
        encoded = urlencode(parse_qs(rid.query), doseq=True)
        return "?".join([out, encoded])
    return out


def from_identifier(rid, request=None):
    kind = rid.hostname
    config_id = int(rid.path[1:])
    rel_url = reverse(prefix("get_geojson", "-"), args=[config_id, kind])
    if request is not None:
        return with_query(request.build_absolute_uri(rel_url), rid)
    return with_query(rel_url, rid)


urlpatterns = [
    path(prefix("step-upload"), get_step_upload, name=prefix("step-upload", "-")),
    path(
        prefix("post-upload"),
        post_upload,
        name=prefix("post-upload", "-"),
    ),
    path(prefix("step-config"), get_step_config, name=prefix("step-config", "-")),
    path(prefix("save-config"), post_config, name=prefix("save-config", "-")),
    path(prefix("preview/<int:project>/"), get_preview, name=prefix("preview", "-")),
    path(
        prefix("config/<int:config_id>/<kind>/"),
        get_geojson,
        name=prefix("get_geojson", "-"),
    ),
    path(prefix("post-alias"), post_alias, name=prefix("post-alias", "-")),
    path(prefix("save-rows"), save_rows, name=prefix("save-rows", "-")),
    # path(
    #     prefix("create-metadata"), create_metadata, name=prefix("create-metadata", "-")
    # ),
]
