#  Copyright (C) 2024 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from pathlib import PosixPath
from django.core.management.base import BaseCommand
from openpyxl import load_workbook

from asf.models import (
    FIELD_GEOMETRY,
    FIELD_NUMBER,
    FIELD_TEXT,
    GEOMETRY_CELL_NAME,
    CellGeom,
    CellNumber,
    CellText,
    ImportConfig,
    Row,
)
from asf.reference import Reference


def find_col_index(name, headers):
    for i, header in enumerate(headers):
        if name == header.value.strip():
            return i
    hns = [h.value for h in headers]
    raise Exception(f'Header Not Found: "{name}" in headers {hns}')


class Left:
    def __init__(self, note) -> None:
        self.note = note


class Right:
    def __init__(self, feature) -> None:
        self.feature = feature


def process_row(config, fields, ref, source_row, source_row_index, aliases):
    print("Loading source row", source_row_index)
    # row = Row.objects.create(config=config, index=source_row_index)
    # target_rows.append(row)
    cells = []
    try:
        for name, ty, index in fields:
            value = source_row[index].value
            if ty == FIELD_GEOMETRY:
                if value in aliases:
                    geom = ref.lookup(aliases[value])
                else:
                    geom = ref.lookup(value)
                cells.append(CellGeom(GEOMETRY_CELL_NAME, geom))
                cells.append(CellText(name, value))
            elif ty == FIELD_TEXT:
                cells.append(CellText(name, value))
            elif ty == FIELD_NUMBER:
                cells.append(CellNumber(name, value))
            else:
                raise Exception(f"Failed on row {source_row_index}: {name} / {ty}")

        if len(fields) != len(cells) - 1:
            cols = "|".join([c.name for c in cells])
            print(
                "Incomplete row, skipping",
                source_row_index,
                cols,
            )
            return Left(f"Incomplete row {source_row_index}, {cols}")

        row = Row.objects.create(config=config, index=source_row_index)
        print("<row", row.index)
        for cell in cells:
            cell.row = row
            cell.save()
            print(f"  <{cell.name}", str(cell.value)[:64])

        return Right(row.get_feature())
    except Reference.NotFound as nf:
        name = nf.args[0]
        print(f'Reference Not Found "{name}". Pick one?')
        names = ref.get_names()
        for i, candidate in enumerate(names):
            print(i, f": {candidate}")
        index = int(input("? "))
        aliases[name] = names[index]
        return process_row(config, fields, ref, source_row, source_row_index, aliases)
    except Exception as ex:
        return Left((source_row_index, str(ex)))
        # print("Failed on row", source_row_index, str(ex))


def load_data(config: ImportConfig, country):
    ref = Reference(country)
    # print("Loading", config.file.name)
    wb = load_workbook(filename=config.survey_file.file)
    if wb.active is None:
        raise Exception("No Active Sheet")

    print("Loading rows")
    sheet = wb.active
    headers = list(sheet.iter_rows(max_row=1))[0]
    print("Loading fields")
    fields = [
        (name, ty, find_col_index(name, headers)) for name, ty in config.get_fields()
    ]

    features = []
    errors = []
    target_rows = []
    aliases = {}

    for source_row_index, source_row in enumerate(
        sheet.iter_rows(min_row=2, max_row=2048)
    ):
        r = process_row(config, fields, ref, source_row, source_row_index, aliases)
        if isinstance(r, Left):
            print("Failed on", source_row_index, str(r.note))
            break

    return errors, target_rows, features


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "config", nargs=1, type=int, help="Id of an ImportConfig object"
        )
        parser.add_argument("country", nargs=1, help="Name of the country")

    def handle(self, *args, **options):
        config_id = options["config"][0]
        config = ImportConfig.objects.get(id=config_id)
        country = PosixPath(options["country"][0])

        try:
            load_data(config, country)
        except Exception as ex:
            self.stderr.write(self.style.ERROR("Errored on: {}".format(ex)))

        # cache = caches["layers"]

        # with config.open() as lines:
        #     for ckey in lines.read().split("\n"):
        #         # print('[{}]'.format(ckey))
        #         try:
        #             schema, table = ckey.split(".")
        #             start = datetime.now()
        #             # the following comes from postgis_loader.views
        #             # which consumes this cache item, so check there if changing here
        #             # or here if changes happened there
        #             stream = io.BytesIO()
        #             writer = codecs.getwriter("utf-8")(stream)
        #             data = get_geojson(schema, table)
        #             dump(data, writer)
        #             stream.seek(0)
        #             cache.set(ckey, stream, read=True)
        #             self.stdout.write(
        #                 self.style.SUCCESS(
        #                     'Got "{}" in cache in {} seconds'.format(
        #                         ckey, (datetime.now() - start).total_seconds()
        #                     )
        #                 )
        #             )
        #         except Exception as ex:
        #             self.stdout.write(
        #                 self.style.ERROR("{} failed: {}".format(ckey, ex))
        #             )
